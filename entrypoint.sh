#!/bin/bash

# if config (share folder) empty, copy tor config back to etc
if [ ! "$(ls -A /etc/tor)" ]; then
  # Empty
  cp -R /init/tor/* /etc/tor
fi

# if no param, start tor
if [ ! $1 ]; then
  echo "Starting Tor ..."
  tor
fi

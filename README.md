# README #
Using Tor Node with Docker on Raspberry Pi (RPi)

### Install Docker on RPi ###

Use the RPi image from hypriot.com.

### Build Docker image ###
```sh
$ ./build-image
```

### Run image ###
```sh
$ ./tart-docker
```

### Seen command log ###
```sh
$ docker logs -f tor-container
```



